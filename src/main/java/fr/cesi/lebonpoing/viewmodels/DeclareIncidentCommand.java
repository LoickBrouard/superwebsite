package fr.cesi.lebonpoing.viewmodels;

import groovy.transform.ToString;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import org.springframework.stereotype.Component;

@ToString
@NoArgsConstructor
@Setter
@Getter
@Component
public class DeclareIncidentCommand {
    public long incidentId;
    public float latitude;
    public float longitude;
    public double radius;

    public boolean CheckIncidentCommand(){
        return incidentId != 0 &&
                latitude != 0 &&
                longitude != 0 &&
                radius <=50 &&
                radius >=10 &&
                radius % 10 == 0;
    }
}
