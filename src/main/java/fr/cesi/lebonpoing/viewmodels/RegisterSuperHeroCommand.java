package fr.cesi.lebonpoing.viewmodels;

import fr.cesi.lebonpoing.models.Incident;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

import java.util.Set;

@ToString
@NoArgsConstructor
@Setter
@Getter
@Component
public class RegisterSuperHeroCommand {
    public String name;
    public String phoneNumber;
    public String description;
    public float latitude;
    public float longitude;
    public Set<Incident> incidents;
    public String success;
    public String error;
}
