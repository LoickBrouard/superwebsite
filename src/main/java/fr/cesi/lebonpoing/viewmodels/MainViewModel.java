package fr.cesi.lebonpoing.viewmodels;

import fr.cesi.lebonpoing.models.Incident;

import java.util.List;

public class MainViewModel {
    public MainViewModel(List<Incident> incidents) {
        this.incidents = incidents;
    }

    public List<Incident> incidents;

}
