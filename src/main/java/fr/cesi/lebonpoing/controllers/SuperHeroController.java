package fr.cesi.lebonpoing.controllers;

import fr.cesi.lebonpoing.models.SuperHero;
import fr.cesi.lebonpoing.services.IIncidentService;
import fr.cesi.lebonpoing.services.ISuperHeroService;
import fr.cesi.lebonpoing.viewmodels.RegisterSuperHeroCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import java.net.URI;
import java.net.URISyntaxException;


@Controller
public class SuperHeroController {

    @Autowired
    ISuperHeroService superHeroService;
    @Autowired
    IIncidentService incidentService;

    @GetMapping(value = "/super/register")
    public ModelAndView index(String flashMessageSuccess, String flashMessageError, RegisterSuperHeroCommand registerSuperHeroCommand) {
        ModelAndView modelAndView = new ModelAndView("SuperHeroView");

        registerSuperHeroCommand = (registerSuperHeroCommand == null) ? new RegisterSuperHeroCommand() : registerSuperHeroCommand;

        modelAndView.addObject("RegisterSuperHeroCommand", registerSuperHeroCommand);
        modelAndView.addObject("incidentList", incidentService.GetAll());

        // In case of flash message
        if (flashMessageSuccess != null) {
            modelAndView.addObject("flashMessageSuccess", flashMessageSuccess);
        }
        if (flashMessageError != null) {
            modelAndView.addObject("flashMessageError", flashMessageError);
        }
        return modelAndView;
    }

    @PostMapping(value = "/super/register")
    public ModelAndView RegisterSuperHero(@ModelAttribute RegisterSuperHeroCommand registerSuperHeroCommand) {
        String iconUrl = "/img/fist.png";

        // Validation
         if(registerSuperHeroCommand.incidents.size() > 3){
             return index(null, "Even if you are SuperMan you cannot handle more than 3 incident ", registerSuperHeroCommand);
         }

         if(registerSuperHeroCommand.incidents.size() < 1){
             return index(null, "I recognize you, you shall not pass ", registerSuperHeroCommand);
         }
        if(registerSuperHeroCommand.name == null || registerSuperHeroCommand.name.trim() == ""){
            return index(null, "We need your name.", registerSuperHeroCommand);
        }

        if(registerSuperHeroCommand.latitude == 0 || registerSuperHeroCommand.longitude == 0 ){
            return index(null, "Something went wrong with the coordinate, please retry to click on the map", registerSuperHeroCommand);
        }
        if(registerSuperHeroCommand.phoneNumber == null || registerSuperHeroCommand.phoneNumber.trim() == ""){
            return index(null, "We need your phone number to reach you in case of trouble.", registerSuperHeroCommand);
        }
        if(registerSuperHeroCommand.description == null || registerSuperHeroCommand.description.trim() == ""){
            return index(null, "Don't be shy, describe yourself a bit :)", registerSuperHeroCommand);
        }

        SuperHero superHeroToAdd = new SuperHero(registerSuperHeroCommand.name,
                registerSuperHeroCommand.latitude,
                registerSuperHeroCommand.longitude,
                HtmlUtils.htmlEscape(registerSuperHeroCommand.phoneNumber.trim()),
                HtmlUtils.htmlEscape(registerSuperHeroCommand.description.trim()),
                iconUrl,
                registerSuperHeroCommand.incidents);
        try {
            superHeroService.AddSuperHero(superHeroToAdd);

        } catch (Exception e) {
            return index(null, "Something went wrong on our side, sorry about that", registerSuperHeroCommand);
        }

        return index("registration success", null, registerSuperHeroCommand);
    }
}