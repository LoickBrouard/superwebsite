package fr.cesi.lebonpoing.controllers;

import fr.cesi.lebonpoing.models.Circle;
import fr.cesi.lebonpoing.models.Incident;
import fr.cesi.lebonpoing.models.SuperHero;
import fr.cesi.lebonpoing.services.IIncidentService;
import fr.cesi.lebonpoing.services.ISuperHeroService;
import fr.cesi.lebonpoing.viewmodels.DeclareIncidentCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/api")
public class ApiController {
    @Autowired
    ISuperHeroService superHeroService;
    @Autowired
    IIncidentService incidentService;

    @GetMapping(path="/super/all"
            ,produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SuperHero> getAll() {
        List<SuperHero> superHeroes = superHeroService.GetAllSuperHeroes();
        return superHeroes;
    }

    @PostMapping(path="/incident/declare")
    public ResponseEntity<List<SuperHero>> getAllSuperHeroesMatchingLocationAndIncident(@RequestBody DeclareIncidentCommand declareIncidentCommand) {
        if(declareIncidentCommand.CheckIncidentCommand()){
            Circle circle = new Circle(declareIncidentCommand.latitude, declareIncidentCommand.longitude, declareIncidentCommand.radius);
            Incident incident = incidentService.GetIncidentFromId(declareIncidentCommand.incidentId);
            return new ResponseEntity(superHeroService.getAllSuperHeroesMatchingLocationAndIncident(circle, incident), HttpStatus.OK);
        }
        else{
            return new ResponseEntity("Invalid request parameters", HttpStatus.BAD_REQUEST);
        }
    }


}
