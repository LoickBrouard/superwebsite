package fr.cesi.lebonpoing.controllers;

import fr.cesi.lebonpoing.services.IIncidentService;
import fr.cesi.lebonpoing.services.ISuperHeroService;
import fr.cesi.lebonpoing.viewmodels.MainViewModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Autowired
    ISuperHeroService superHeroService;
    @Autowired
    IIncidentService incidentService;

    @RequestMapping(value = {"/index", "/"})
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("HomeView");
        modelAndView.addObject(new MainViewModel(incidentService.GetAll()));
        return modelAndView;
    }

    @RequestMapping(value = {"/sources"})
    public String sourcesView() {
        return "SourceListView";
    }
}