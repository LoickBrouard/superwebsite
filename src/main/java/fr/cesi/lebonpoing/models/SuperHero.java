package fr.cesi.lebonpoing.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class SuperHero implements java.io.Serializable {
    public SuperHero() {
    }

    public SuperHero(String name, float latitude, float longitude, String phoneNumber, String description, String iconUrl, Set<Incident> incidents) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phoneNumber = phoneNumber;
        this.description = description;
        this.iconUrl = iconUrl;
        this.incidents = incidents;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "hero_id", nullable = false)
    public Long id;
    @Column(nullable = false)
    public String name;
    @Column(nullable = false)
    public float latitude;
    @Column(nullable = false)
    public float longitude;
    @Column(name = "phone_number")
    public String phoneNumber;
    @Column(name = "description")
    public String description;
    @Column(name = "icon_url")
    public String iconUrl;


    @ManyToMany(cascade = {CascadeType.ALL})
    @JoinTable(
            name = "hero_incident",
            joinColumns = {@JoinColumn(name = "hero_id")},
            inverseJoinColumns = {@JoinColumn(name = "incident_id")}
    )
    public Set<Incident> incidents = new HashSet<>();

    // Overriding the toString method
    // to find all the values
    @Override
    public String toString() {

        return "SuperHero [id="
                + id + ", name="
                + name + ", latitude="
                + latitude + ", longitude="
                + longitude + "]";
    }

}