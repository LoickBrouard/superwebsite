package fr.cesi.lebonpoing.models;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Incident {

    public Incident() { }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="incident_id", nullable = false)
    public Long id;

    @Column(nullable = false)
    public String name;

    @Column(name="icon_url", nullable = false)
    public String iconUrl;

    @ManyToMany(mappedBy = "incidents", cascade = { CascadeType.ALL})
    private Set<SuperHero> heroes = new HashSet<>();
}
