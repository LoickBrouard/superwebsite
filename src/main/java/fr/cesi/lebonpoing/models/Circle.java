package fr.cesi.lebonpoing.models;

import static java.lang.Math.*;

public class Circle {
    public float LatitudeCenter;
    public float LongitudeCenter;
    public double RadiusInKiloMeter;

    public Circle(float latitudeCenter, float longitudeCenter, double radiusInKiloMeter) {
        LatitudeCenter = latitudeCenter;
        LongitudeCenter = longitudeCenter;
        RadiusInKiloMeter = radiusInKiloMeter;
    }

    public Circle(float latitudeCenter, float longitudeCenter) {
        LatitudeCenter = latitudeCenter;
        LongitudeCenter = longitudeCenter;
        RadiusInKiloMeter = 50;
    }

    public boolean isPositionInCircle(float latitudeToCheck, float longitudeToCheck) {
        float distanceInKilometer = (float) (acos(sin(toRadians(this.LatitudeCenter))*sin(toRadians(latitudeToCheck))+cos(toRadians(this.LatitudeCenter))*cos(toRadians(latitudeToCheck))*cos(toRadians(this.LongitudeCenter-longitudeToCheck)))*6371);
        return distanceInKilometer < this.RadiusInKiloMeter;
    }
}
