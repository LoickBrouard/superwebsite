package fr.cesi.lebonpoing.services;

import fr.cesi.lebonpoing.models.Circle;
import fr.cesi.lebonpoing.models.Incident;
import fr.cesi.lebonpoing.models.SuperHero;
import fr.cesi.lebonpoing.repositories.ISuperHeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SuperHeroService implements ISuperHeroService{
    @Autowired
    ISuperHeroRepository superHeroRepository;

    public SuperHero AddSuperHero(SuperHero superHero){
        return superHeroRepository.save(superHero);
    }

    public SuperHero GetSuperHeroById(Long id) {
        return null;
    }

    public List<SuperHero> GetAllSuperHeroes() {
        return superHeroRepository.findAll();
    }

    public List<SuperHero> getAllSuperHeroesMatchingLocationAndIncident(Circle circle, Incident incident) {
        return superHeroRepository
                .findAll()
                .stream()
                .filter(s->
                        circle.isPositionInCircle(s.latitude, s.longitude) == true &&
                        s.incidents.contains(incident))
                .collect(Collectors.toList());
    }
}
