package fr.cesi.lebonpoing.services;

import fr.cesi.lebonpoing.models.Incident;
import fr.cesi.lebonpoing.repositories.IIncidentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class IncidentService implements IIncidentService{
    @Autowired
    IIncidentRepository incidentRepository;

    public Incident GetIncidentFromId(Long incidentId) {
        return incidentRepository.findIncidentById(incidentId);
    }

    public List<Incident> GetAll() {
        return incidentRepository.findAll();
    }
}
