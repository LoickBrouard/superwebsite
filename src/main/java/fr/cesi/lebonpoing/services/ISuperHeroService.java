package fr.cesi.lebonpoing.services;

import fr.cesi.lebonpoing.models.Circle;
import fr.cesi.lebonpoing.models.Incident;
import fr.cesi.lebonpoing.models.SuperHero;

import java.util.List;

public interface ISuperHeroService {
    SuperHero AddSuperHero(SuperHero superHero);
    SuperHero GetSuperHeroById(Long id);
    List<SuperHero> GetAllSuperHeroes();
    List<SuperHero> getAllSuperHeroesMatchingLocationAndIncident(Circle circle, Incident incident);
}
