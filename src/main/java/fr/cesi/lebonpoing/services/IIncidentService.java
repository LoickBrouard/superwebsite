package fr.cesi.lebonpoing.services;

import fr.cesi.lebonpoing.models.Incident;

import java.util.List;

public interface IIncidentService {
    public Incident GetIncidentFromId(Long id);
    public List<Incident> GetAll();
}
