package fr.cesi.lebonpoing.repositories;

import fr.cesi.lebonpoing.models.SuperHero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISuperHeroRepository extends JpaRepository<SuperHero, Long> {
}