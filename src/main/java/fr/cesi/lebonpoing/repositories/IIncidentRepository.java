package fr.cesi.lebonpoing.repositories;

import fr.cesi.lebonpoing.models.Incident;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IIncidentRepository extends JpaRepository<Incident, Long> {
    Incident findIncidentById(Long id);
}
