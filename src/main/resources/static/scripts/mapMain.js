//*******************************//
//        VARIABLES              //
//*******************************//

// DOM elements
let leftContent = document.getElementById("left-content");

// Markers
let currentMarker = null;
let currentCircle = null;
let currentLatitude = null;
let currentLongitude = null;

// Api url
const apiBaseUrl = "http://127.0.0.1:8080/api";

let defaultLatitude = 49.452609;
let defaultLongitude = 1.080136;
let zoomLevel = 50;

// Creating map default options
// By default Rouen is aimed
let mapOptions = {
    center: [49.4431300, 1.0993200],
    zoom: 10,
    zoomControl: false,
}

// Defining the mousse onclick Icon
let defaultIncidentIcon = L.icon({
    iconSize:[40,40],
    iconUrl: "/img/target.png"
});


//*******************************//
//      MAP GENERATION           //
//*******************************//

// Creating a map object
let map = new L.map('map', mapOptions);

// Creating a Layer object
let layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

// Adding layer to the map
map.addLayer(layer);

// Setting navigation options (zoom, left/right navigation)
map.setMinZoom(3);
map.setMaxBounds([
    [90, -180],
    [-90, 180  ]
]);

//*******************************//
//        PAGE LOADING           //
//*******************************//

// Prevent the js script to execute all code on page loading
document.addEventListener("load", (e) =>{
    e.preventDefault();
})

// Start the map loading
GetLoc();

//*******************************//
//   GEOLOCALIZATION FUNC.       //
//*******************************//

// Geolocalisation
function GetLocSuccess(pos) {
    let crd = pos.coords;
    if(crd.latitude != "" && crd.longitude != "" && zoomLevel != ""){
        currentLatitude = crd.latitude;
        currentLongitude = crd.longitude;
        mapOptions.center = [crd.latitude, crd.longitude];
        mapOptions.zoom = zoomLevel;
    }else{
        mapOptions.center = [currentLatitude,currentLongitude];
    }
    currentMarker = L.marker([currentLatitude, currentLongitude], {icon:defaultIncidentIcon, incidentId:0, title:'You are here'})
    currentMarker.addTo(map);
    map.flyTo([currentLatitude, currentLongitude], 15);
}

// On error on geolocalisation Rouen is aimed
function GetLocError(err) {
    console.warn(`ERREUR (${err.code}): ${err.message}`);
    if(defaultLatitude != "" && defaultLongitude != "" && zoomLevel != ""){
        currentLatitude = defaultLatitude;
        currentLongitude = defaultLongitude;
    }
    currentMarker = L.marker([currentLatitude, currentLongitude], {icon:defaultIncidentIcon, incidentId:0, title:'You are here'})
    currentMarker.addTo(map);
    map.flyTo([currentLatitude, currentLongitude], 15);
}

// Geo function
function GetLoc(){
    navigator.geolocation.getCurrentPosition(GetLocSuccess, GetLocError);
}

//*******************************//
//          MAP FUNC.            //
//*******************************//

function SuperHeroMarkerFactory(latitude, longitude, superHeroIconUrl, superHeroId, superHeroName){
    return L.marker([latitude, longitude], {
        icon: CreateSuperHeroMarker(superHeroIconUrl),
        heroId: superHeroId,
        title: superHeroName
    });
}

function addCircle(radiusInKilometer){
    // Displaying circle marker
    if(currentCircle != null){ currentCircle.remove(); }
    currentCircle = L.circle([currentLatitude, currentLongitude], {radius: radiusInKilometer*1000});
    currentCircle.addTo(map);
}

// Defining the super hero Icon
function CreateSuperHeroMarker(iconUrl){
    return L.icon({
        iconSize:[40,40],
        iconUrl: iconUrl
    });
}

//*******************************//
//   FONCTIONS INTERAC. MARKER   //
//*******************************//

function createPopupCssForSuperHeroMarkers(superHeroId, superHeroName, superHeroPhoneNumber){
    return `Name : ${superHeroName}<br/>phone : ${superHeroPhoneNumber}<br/><a href="">Profil</a>`;
}

//*******************************//
//   FONCTIONS INTERAC. BACK     //
//*******************************//

function GetSuperHeroLocation(){
    let method = "GET";
    let url = `${apiBaseUrl}/super/all`;
    return SendRequestReturnAPromise(method, url);
};

function SendRequestReturnAPromise(method, url, data = null, headers = []){
    return new Promise((resolve, reject) => {
        xhr = new XMLHttpRequest()
        xhr.open(method, url);
        if(headers.length>0){
            for(let i = 0; i<headers.length; i++){
                xhr.setRequestHeader(headers['name'], headers['value']);
            }
        }
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.response);
            } else {
                reject(xhr.statusText);
            }
        };
        xhr.onerror = () => reject(xhr.statusText);

        if(data != null && method === "POST"){
            xhr.send(data);
        }
        else{
            xhr.send();
        }
    });
}