//*******************************//
//          VARIABLES            //
//*******************************//
let submitRegisterButton = document.getElementById("submitRegisterButton");
let RegisterForm = document.getElementById("RegisterForm");
let latitudeForm = document.getElementById("latitudeForm");
let longitudeForm = document.getElementById("longitudeForm");
let counterCliked = 0;
//*******************************//
//           EVENTS              //
//*******************************//

// Handling on
map.on('click', function (e) {
    // update the current LatLong on click
    currentLongitude = parseFloat(e.latlng["lng"]);
    currentLatitude = parseFloat(e.latlng["lat"]);

    // Displaying target icon
    if (currentMarker != null) {
        currentMarker.remove();
    }
    currentMarker = L.marker([currentLatitude, currentLongitude], {
        icon: incidentIcon,
        incidentId: 0,
        title: 'You are here'
    })
    currentMarker.addTo(map);
});

submitRegisterButton.addEventListener('click', function (e) {
    latitudeForm.value = currentLatitude;
    longitudeForm.value = currentLongitude;
    if(NumberOfCheckboxTicked < 1){
        e.preventDefault();
    }

    if(NumberOfCheckboxTicked < 1 && counterCliked == 0) { alert("You must choose at least a incident to manage to be useful to the community :)"); counterCliked ++; return;}
    if(NumberOfCheckboxTicked < 1 && counterCliked == 1) { alert("Ok, you are clearly not the sharpest tool in the shed ! Please choose an incident");counterCliked ++; return;}
    if(NumberOfCheckboxTicked < 1 && counterCliked == 2) { alert("The light is on but nobody’s home... Please click on one of the freakin' checkboxes !!!");counterCliked ++; return;}
    if(NumberOfCheckboxTicked < 1 && counterCliked == 3) { alert("Please ask someone to handle this task for you buddy...");counterCliked ++; return;}
    if(NumberOfCheckboxTicked < 1 && counterCliked == 4) { alert("Nice try but no ! I know this button is catchy, it's on me. Try again ^^");counterCliked ++; return;}
    if(NumberOfCheckboxTicked < 1 && counterCliked == 5)
    {
        alert("Ok you know what I'll move you somewhere safe, just in case you finally subscribe by chance...");
        currentLatitude = 83.294785;
        currentLongitude = -26.653267;

        map.flyTo([currentLatitude, currentLongitude], 5);
        return;
    }
    counterCliked = 0;

})

let limit = 3;//Limit of selections
let NumberOfCheckboxTicked = document.querySelectorAll(".incidentCheckBox:checked").length;//Selection counter

//I detect when a common element changes
document.querySelectorAll(".incidentCheckBox").forEach((elt) => elt.addEventListener('change', (e) => {

    //Check if the item is checked
    if (e.target.checked) {
        if (NumberOfCheckboxTicked < limit) {
            NumberOfCheckboxTicked = parseInt(NumberOfCheckboxTicked) + 1;
        } else {
            e.target.checked = false; // Unchecks it
        }
    }
    // If the checkbox is not checked
    else {
        NumberOfCheckboxTicked = parseInt(NumberOfCheckboxTicked) - 1;
    }
}))