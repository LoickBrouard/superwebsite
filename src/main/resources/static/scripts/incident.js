//*******************************//
//        VARIABLES              //
//*******************************//

let HideLeftContentButton = document.getElementById("HideLeftContentButton");
let radius = document.getElementById("radius");
let declareIncidentButton = document.getElementById("declareIncidentButton");
let incidentDeclared = document.getElementById("incidentDeclared");
let superHeroList = document.getElementById("superHeroList");

let currentIncidentIcon = null;

let tempSuperHeroMarkers = [];
//*******************************//
//           EVENTS              //
//*******************************//

HideLeftContentButton.addEventListener("click", (e) => {
    leftContent.style.visibility = "collapse";

    // Clearing map from markers
    if (currentMarker != null) {
        currentMarker.remove();
        currentMarker = null;
    }
    if (currentCircle != null) {
        currentCircle.remove();
        currentCircle = null;
    }
    if (superHeroList.innerHTML != "") {
        superHeroList.innerHTML = "";
    }
    currentIncidentIcon = null;

    clearDisplayedSuperHero();
})

radius.addEventListener("change", (e) => {
    addCircle(e.target.value);
})

declareIncidentButton.addEventListener("click", (e) => {

    if (currentLatitude == null || currentLongitude == null) {
        alert("You need to click on the map to place a marker on the map first");
        return;
    }
    // Radius of 50km by default
    if (radius.value == null || radius.value == "NoSelection") {
        radiusSelected = 50;
    } else {
        radiusSelected = radius.value;
    }
    if (incidentDeclared.value == null || incidentDeclared.value == "NoSelection") {
        alert("You need to choose an incident type");
        return;
    }

    let superHeroResultList = GetSuperHeroesMatchingIncidentAndLocation(currentLatitude, currentLongitude, incidentDeclared.value, radiusSelected);

    superHeroResultList.then((response) => {
        return response.json();
    })
        .then((superHeroReturned) => {
            superHeroList.innerHTML = "";
            tempSuperHeroMarkers.forEach((elt)=> elt.remove());
            if (superHeroReturned.length > 0) {
                for (let i = 0; i < superHeroReturned.length; i++) {

                    let incidentHandled = `<div class="flex">`;
                    for (let j = 0; j < superHeroReturned[i]["incidents"].length; j++) {

                        incidentHandled += `<img class="w-5 h-5 mx-1 mb-4" src="${superHeroReturned[i]["incidents"][j]["iconUrl"]}" alt="${superHeroReturned[i]["incidents"][j]["name"]}" title="${superHeroReturned[i]["incidents"][j]["name"]}" />`;
                    }

                    incidentHandled += `</div>`;

                    superHeroList.innerHTML += `<div class="p-4 lg:w-auto md:w-full">
                                           <div class="flex-col border-2 rounded-lg border-gray-200 border-opacity-50 p-8 sm:flex-row flex-col">
                                                <div class="w-16 h-16 sm:mr-8 sm:mb-0 mb-4 inline-flex items-center justify-center rounded-full bg-green-100 text-green-500 flex-shrink-0">
                                                    <img src="${superHeroReturned[i]["iconUrl"]}" alt="iconUrl">
                                                </div>
                                            <div class="flex-grow">
                                            <h2 class="text-gray-900 text-lg title-font font-medium mb-3">${superHeroReturned[i]["name"]}</h2>
                                            ${incidentHandled}
                                            <p class="leading-relaxed text-base">${superHeroReturned[i]["description"]}</p>
                                            <p class="mt-3 text-green-500 inline-flex items-center">${superHeroReturned[i]["phoneNumber"]}</p>
                                            </div>
                                            </div>
                                            </div>`;


                    let tempMarker = SuperHeroMarkerFactory(superHeroReturned[i]["latitude"],
                        superHeroReturned[i]["longitude"],
                        superHeroReturned[i]["iconUrl"],
                        superHeroReturned[i]["id"],
                        superHeroReturned[i]["name"])
                    tempMarker.addTo(map)
                        .bindPopup(createPopupCssForSuperHeroMarkers(superHeroReturned[i]["id"],
                            superHeroReturned[i]["name"],
                            superHeroReturned[i]["phoneNumber"]));
                    tempSuperHeroMarkers.push(tempMarker);

                }
            } else {
                superHeroList.innerHTML = `<div class="p-4 lg:w-auto md:w-full">
        <div class="flex-col border-2 rounded-lg border-gray-200 border-opacity-50 p-8 sm:flex-row flex-col">
            <div
                class="w-16 h-16 sm:mr-8 sm:mb-0 mb-4 inline-flex items-center justify-center rounded-full bg-green-100 text-green-500 flex-shrink-0">
                <img src="/img/sad.png" alt="iconUrl">
            </div>
            <div class="flex-grow">
                <h2 class="text-gray-900 text-lg title-font font-medium mb-3">No super hero found :'(<br/> You're on your own, sorry...</h2>
            </div>
        </div>
    </div>`
            }
        })
        .catch((e) => {
            alert(e);
        });
})

incidentDeclared.addEventListener('change', (e) => {
    var index = document.querySelector('#incidentDeclared').selectedIndex - 1;
    let optionText = document.querySelectorAll(".incidentOption");
    let incidentIcon = optionText[index].getAttribute("alt");

    if(currentMarker != null){ currentMarker.remove(); }

    currentIncidentIcon = L.icon({
        iconSize:[40,40],
        iconUrl: incidentIcon
    });

    currentMarker = L.marker([currentLatitude, currentLongitude], {icon:currentIncidentIcon, incidentId:0, title:'You are here'})
    currentMarker.addTo(map);


});
//*******************************//
//           FUNCTIONS.          //
//*******************************//

function GetSuperHeroesMatchingIncidentAndLocation(latitude, longitude, incidentId, radius) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const body = `{
  "incidentId":${parseInt(incidentId)},
  "latitude": ${latitude},
  "longitude":${longitude},
  "radius":${parseFloat(radius)}
  }`;

    const init = {
        method: 'POST',
        headers,
        body
    };

    return fetch(`${apiBaseUrl}/incident/declare`, init);
}
function clearDisplayedSuperHero(){
    if(tempSuperHeroMarkers != null && tempSuperHeroMarkers.length > 0){
        tempSuperHeroMarkers.forEach((elt)=> {
            elt.remove();
        })
    }
}
