//*******************************//
//        PAGE LOADING           //
//*******************************//
// Create different layers
var allSuperHeroLayerGroup = L.layerGroup([]);

var baseMaps = { "SuperHero": layer  };

var overlayMaps = { "All super heroes": allSuperHeroLayerGroup };

L.control.layers(baseMaps, overlayMaps, {hideSingleBase : true, position : 'bottomleft'}).addTo(map);

GetSuperHeroLocation().then((result)=> {
    result = JSON.parse(result);

    for(let i=0; i < result.length; i++){
        SuperHeroMarkerFactory(result[i]["latitude"],
            result[i]["longitude"],
            result[i]["iconUrl"],
            result[i]["id"],
            result[i]["name"]).addTo(allSuperHeroLayerGroup).bindPopup(createPopupCssForSuperHeroMarkers(result[i]["id"], result[i]["name"], result[i]["phoneNumber"]));
    }
});

//*******************************//
//           EVENTS              //
//*******************************//

// Handling on
map.on('click', function(e) {
    // update the current LatLong on click
    currentLongitude = parseFloat(e.latlng["lng"]);
    currentLatitude = parseFloat(e.latlng["lat"]);

    iconToDisplay = (currentIncidentIcon != null)? currentIncidentIcon : defaultIncidentIcon;

    // Displaying target icon
    if(currentMarker != null){ currentMarker.remove(); }
    currentMarker = L.marker([currentLatitude, currentLongitude], {icon:iconToDisplay, incidentId:0, title:'You are here'})
    currentMarker.addTo(map);

    if(currentCircle != null && radius != null){
        currentCircle.remove();
        addCircle(radius.value);
    }

    // Displaying declare incident panel
    leftContent.style.visibility = "visible";
} );
